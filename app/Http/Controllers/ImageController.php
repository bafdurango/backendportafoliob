<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Intervention\Image\Facades\Image;
use Storage;
use Log;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;


class ImageController extends Controller
{

	public $storage_disk;

	public function __construct() {

		$this->storage_disk_name = "public_uploads";
		$this->storage_disk = Storage::disk($this->storage_disk_name);
	}

	public function index()
    {

    	//$url = "https://www.xataka.com/legislacion-y-derechos/huella-dactilar-no-suficiente-policia-nacional-incorporara-datos-faciales-al-dni-electronico-mediante-nuevo-sistema-biometrico";

    	Log::info(is_null(request('post_url')));

    	$post_url = request('post_url');

    	$html = null;

    	if(!is_null($post_url)){
    		$post_url = urldecode($post_url);

    		$html = $this->crawlPost($post_url);
    	}

    	$columns = isset(request()->columns)?request()->columns:4;

        $files = $this->storage_disk->files();

        $files = $this->onlyImages($files);

        foreach ($files as $key => $value) {
        	$files[$key] = $this->storage_disk->url($value);
        }

        $table = [];

        $td = 1;
        $tr = 1;

        foreach ($files as $file) {

        	$table[$tr][$td] = $file;

        	if(($td % $columns) == 0) {
        		$tr++;
        	}

        	$td++;
        }

        $data = [
        	'table' => $table,
        	'html' => $html
        ];

        return view('image.index', compact('data'));
    }

    public function upload()
    {

        $hashName = request()->file('image')->hashName();

        request()->file('image')->storeAs(null, $hashName,['disk' => $this->storage_disk_name]);


        return redirect(route('index'));

    }

    public function onlyImages($files) {

        $files = array_filter( $files, function ($file) {

        	$supported_image = array(
			    'gif',
			    'jpg',
			    'jpeg',
			    'png'
			);

        	$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        	return in_array($ext, $supported_image);

        });

        return $files;

    }

    public function deleteAll() {

    	$files = $this->storage_disk->files();

        $files = $this->onlyImages($files);

    	$this->storage_disk->delete($files);

    	return redirect()->route('index');
    }

    public function watermark(Request $request) {

    	Log::info($request);

    	//Log::info($table);

    	$files = $this->storage_disk->files();

        $file = public_path('uploads/' . array_shift($files));

        Log::info($file);

    	$image = Image::make($file);

    	$wm_path = public_path('logo.png');

    	$wm_imagen = Image::make($wm_path);

    	$wm_updated_path = public_path('uploads/watermarkupdated.png');

    	Log::info($wm_updated_path);

    	$wm_imagen_updated = $wm_imagen->opacity(50)->save($wm_updated_path);

    	//Log::info($image->size());
    	//dd($image->width());

    	$x_porcentaje = $request->x_porcentaje;
    	$y_porcentaje = $request->y_porcentaje;

    	$x_porcentaje = intval($x_porcentaje);
    	$y_porcentaje = intval($y_porcentaje);

    	$x_pos_px = ($image->width() * $x_porcentaje ) / 100;
    	$y_pos_px = ($image->height() * $y_porcentaje ) / 100;

    	$x_pos_px = $x_pos_px - ($wm_imagen_updated->width() / 2);
    	$y_pos_px = $y_pos_px - ($wm_imagen_updated->height() / 2);;

    	$x_pos_px = round($x_pos_px);
    	$y_pos_px = round($y_pos_px);

    	$image->insert($wm_imagen_updated,null,$x_pos_px,$y_pos_px)->save();

    	return redirect(route('index'));
    }

    public function cut(Request $request) {

    	Log::info('cut');

    	Log::info($request->all());

    	Log::info(intval($request->x_partes));

    	$x_pieces = intval($request->x_pieces);
    	$y_pieces = intval($request->y_pieces);

    	//Log::info($x_partes);
    	//Log::info($y_partes);

    	//crop(int $width, int $height, int $x = null, int $y = null)

    	$files = $this->storage_disk->files();
    	$files = $this->onlyImages($files);

        $file_path = array_shift($files);
        $absolute_file_path = public_path('uploads/' . $file_path);

        $image = Image::make($absolute_file_path);

        $this->storage_disk->delete($file_path);

        Log::info($image->width());
        Log::info($image->width() / 2);
        

        $image->backup();

        //$x_pieces = request()->x_pieces;
        //$y_pieces = request()->y_pieces;

        $width_piece = $image->width() / $x_pieces;
        $height_piece = $image->height() / $y_pieces;

        
        $count_piece = 10;

        //$tr = 1;

        for ($tr=0; $tr < $y_pieces; $tr++) {

        	Log::info('tr');

        	//$td = 1;

        	for ($td=0; $td < $x_pieces; $td++) {

        		Log::info('td');

        		$x_origin_point = $td * $width_piece;
        		$y_origin_point = $tr * $height_piece;


		    	$image->crop($width_piece, $height_piece,$x_origin_point,$y_origin_point)->save('uploads/'.$count_piece.'_piece.jpg');

		    	$count_piece++;

		    	Log::info($image->width());

		    	$image->reset();

        		//$td++;
        	}

        	//$tr++;
        }

    	Log::info($image->width());

    	return redirect(route('index', ['columns' => $x_pieces]));
    }

    public function crawlPost($url){

    	$client = new Client();

    	$res = $client->request('GET', $url);

    	$html = $res->getBody()->getContents();

    	//Log::info($html);

    	//$crawler = new Crawler($html);
    	$crawler = new Crawler();

    	$crawler->addHTMLContent($html,'UTF-8');

		$filtrados = $crawler->filter('h1,h2,p,img')->each(function($node){

			$domElement = $node->getNode(0);

			Log::info($node->text());

			if ($domElement->nodeName == 'img') {

				$style = "max-width: 50%;";

				$domElement->setAttribute('style',$style);

				$domElement->removeAttribute('width');
				$domElement->removeAttribute('height');
			}

			if ($domElement->nodeName == 'p') {

				$style = "font-size: 1.2em;";

				$domElement->setAttribute('style',$style);
			}

			$domDocument = $domElement->ownerDocument;

			$html = $domDocument->saveHTML($domElement);

			return $html;

		});

    	$html_post = implode(" ", $filtrados);

    	//Log::info($html_post);
    	//Log::info('información');

    	return $html_post;

    }

}