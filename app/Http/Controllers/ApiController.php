<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Intervention\Image\Facades\Image;
use Storage;
use Log;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;


class ApiController extends Controller
{

	public function __construct() {

	}

	public function index()
    {

    	$posts = $this->getPosts();

        return response()->json($posts);
    }

    public function getPost($id) {

        $posts = $this->getPosts();

        $post = null;

        foreach ($posts as $value) {
            if ($value['id'] == $id) {
                $post = $value;
            }
        }

        return response()->json($post);
    }


    public function getPosts() {

        $posts = array(

            array(
                'id' => "1",
                'title' => "Cómo encontrar el juego secreto de pinball en la app de Google de iOS",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_1.jpg",
                'content' => "

                <p>Vamos a explicarte <strong>cómo activar y lanzar el juego secreto de pinball</strong> que hay escondido en la app de Google para iOS. Se trata de un simpático huevo de pascua con el que vas a poder echar los ratos libres jugando al pinball en la pantalla de tu móvil. Los gráficos de este juego y sus mecánicas son extremadamente sencillos, pero seguro que puede robarte 5 o 10 minutos.</p>

                <p>Como ya te vas a imaginar, el requisito imprescindible es <strong>tener descargada la app de Google para iOS</strong>, que está disponible <a href='https://apps.apple.com/es/app/google/id284815942'>en la App Store</a>, y tener la app actualizada. La mecánica es sencilla, ya que aparecerán unas figuras de colores en la pantalla, y tendrás que destruirla con las bolas intentando conseguir la máxima puntuación posible.</p>

                    <p>Lo primero que tienes que hacer es entrar en la aplicación de Google en iOS, e ir a su página principal. Una vez dentro, <strong>pulsa en la pestaña de <em>Pestañas</em></strong>, que aparece abajo a la izquierda con el icono de dos ventanas superpuestas.</p>

                    <p>Entrarás en la sección de <em>Pestañas</em>, donde verás todas las páginas que has ido abriendo desde <a href='https://www.xataka.com/basics/google-discover-trucos-consejos-para-dominar-recomendaciones-app-google'>Google Discover</a> en la pestaña <em>Casa</em>, o desde el buscador de Google con la app. Lo que tienes que hacer es <strong>cerrar todas las pestañas que tengas abiertas</strong>, ya sea una a una pulsando en su botón X, o dándole al icono de la papelera que tienes arriba a la derecha para cerrarlas todas a la vez.</p>

                    <p>Cuando cierres todas las pestañas, tienes que esperar unos segundos hasta que en la parte inferior de la pantalla aparezcan unas figuras geométricas de colores. Cuando aparezcan, <strong>desliza estas figuras hacia arriba con el dedo</strong> poco a poco. Para hacerlo, presiona la pantalla con el dedo sobre una, y desliza el dedo hacia arriba sin levantarlo.</p>

                "
            ),

            array(
                'id' => "2",
                'title' => "Las nuevas oficinas de Dropbox se parecen muy poco a unas oficinas convencionales: con paredes y muebles móviles para amoldarse al trabajo híbrido",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_2.png",
                'content' => "

                <p>Entre los modelos híbridos, remotos, de teletrabajo parcial o de vuelta total a la oficina, Dropbox apostó por lo que llamó Virtual First: los empleados trabajarán fundamentalmente desde casa y solo podrán ir a las oficinas cuando haya reuniones.</p>

                <p>Desde ese momento, la compañía anunció que cambiaría sus oficinas y que no descartaba abrir nuevos lugares de trabajo (o Studios, como los llama) en nuevas localizaciones si había un número de empleados suficeinte.</p>

                <h2>Cómo son los nuevos Studios</h2>

                    <p>Seis meses después de aquella decisión, Dropbox ha mostrado cómo son estos nuevos entornos de trabajo, diseñados para el trabajo en equipo de forma física y para que los empleados tengan una conexión personal con otros compañeros.

</p>

                    <p>Algo que, tal y como reconoce la compañía, no solo no se puede replicar virtualmente, sino que se ha echado de menos durante el último año y medio.</p>

                    <p>Dropbox asegura que con estos Studios se quiere rediseñar cómo se usan las oficinas y cómo los empleados pueden colaborar, entre ellos y con clientes o proveedores, a la hora de hacer su trabajo.

</p>

                "
            ),

            array(
                'id' => "3",
                'title' => "54 aplicaciones imprescindibles para tus vacaciones de verano",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_3.jpg",
                'content' => "

                <p>Te traemos una colección de 54 aplicaciones imprescindibles para tus vacaciones de verano, de forma que tengas herramientas suficientes como para estar organizado antes y durante el viaje. Y no, en este artículo no vamos a decirte simplemente que descargues Netflix para entretenerte, sino que te proponemos aplicaciones orientadas hacia los viajes, el entretenimiento más genérico tendrás que ponerlo tú.

</p>

                <p>vamos a dividir el artículo en varios bloques. Empezaremos con las aplicaciones para buscar vuelos y alojamientos, y seguiremos con otras para buscar ideas en el destino al que quieras ir, para moverte dentro y fuera de las ciudades, y utilidades para antes de viajar o mientras lo haces. Luego, acabaremos con otra sección de utilidades para tu destino.

</p>

                    <p>Aunque hemos intentado hacer un artículo atemporal y no lo mencionamos, no te olvides que también conviene obtener el pasaporte COVID si vas a salir del país. Por lo demás, como siempre decimos en Xataka Basics, si conoces alguna aplicación que se nos haya pasado por alto te invitamos a que compartas tus propuestas en la sección de comentarios, y que así todos los lectores se beneficien del conocimiento de nuestros xatakeros.

</p>

    <h2>Para buscar vuelos y alojamientos</h2>

                    <p>Vamos a empezar con un lote de aplicaciones específicamente diseñadas para que puedas buscar y reservar tus vuelos y hoteles, así como coches de alquiler u otros tipos de medios de transporte. Quizá en esta lista haya menos sorpresas en cuanto a los servicios elegidos, pero eso no los hace menos imprescindibles.</p>

                    <p>AirBnb: AirBnb es una de las plataformas que puso de moda los alquileres vacacionales, aunque en los últimos tiempos la pandemia global les ha puesto la cosa complicada. Aun así, sigue siendo una buena alternativa a los clásicos hoteles, que puede resultarte un poco más cómoda y doméstica en algunos casos. Te va a permitir delimitar dónde quieres buscar el apartamento y filtrar algunas de sus características para dar con lo que más te convenza. Tienes su página web y sus aplicaciones para Android e iOS.</p>

                "
            ),

            array(
                'id' => "4",
                'title' => "El plan de la NASA para arreglar el Hubble y su ordenador de hace 47 años",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_4.jpg",
                'content' => "

                <p>Una semana atrás ocurrió la tragedia: el Hubble entró en modo seguro debido a un problema con el ordenador interno. El telescopio espacial que nos ha ofrecido algunos de los descubrimientos astronómicos más importantes de la historia, de repente dejaba de estar operativo. Ahora la NASA tiene un plan para recuperarlo, si es que va según la teoría.</p>

                <p>El ordenador de carga útil del telescopio espacial detuvo sus operaciones el pasado 13 de junio y automáticamente entró en modo seguro. Esto significa que en teoría el telescopio sigue funcionando, pero no va a encenderse ni realizar operaciones hasta que no se arregle el ordenador. Es una medida de precaución que se toma automáticamente para evitar daños mayores.</p>


                    <p>Casi un mes después, el Hubble sigue sin ofrecer señales de vida. Hasta ahora los intentos de la NASA han pasado por reiniciarlo o sustituirlo por el ordenador de sustitución que tiene el Hubble. Y es que dentro del Hubble hay dos ordenadores idénticos, el segundo especialmente puesto ahí en caso de que el primero falle. Ha fallado, pero no hay forma de iniciar el segundo.</p>

                "
            ),

            array(
                'id' => "5",
                'title' => "Qué me recomiendan los expertos en ciberseguridad que estudie para ser un experto en la disciplina",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_5.jpg",
                'content' => "

                <p>Cada vez que salta a los medios la noticia de una importante brecha de seguridad en una gran compañía, la seguridad tecnológica gana en valoración y con ella los expertos en este terreno.</p>

                <p>Esto está provocando que la demanda de expertos en seguridad aumente de tal manera que se quedan sin cubrir 3 millones de puestos en todo el mundo, según el informe Cybersecurity Workforce Study de (ISC)², una organización mundial de profesionales en seguridad informática.</p>


                    <p>¿Consecuencia de esto? Que, pese a que el perfil de experto en ciberseguridad no sea el más demandado, sí es uno de los dos más cotizados según el Informe del Mercado Laboral de Adecco: la posición de CISO (Chief Information Security Officer, jefe de seguridad) tiene un salario que, según este informe, oscila entre los 45.000 y los 95.000 euros en nuestro país.</p>

                "
            ),

            array(
                'id' => "6",
                'title' => "Dubai se lleva el récord de la piscina más profunda del mundo con un vertiginoso fondo de 60 metros",
                'image' => "http://backendportafoliob.herokuapp.com/posts_images/post_6.jpg",
                'content' => "

                <p>En estos últimos años nos hemos visto sumergidos (nunca mejor dicho) en lo que parece una tímida competición por lograr la piscina con mayor profundidad. Parecía que Blue Abbys iba a sostener este oro durante al menos un tiempo, pero ha tenido que venir Dubai a llevarse el récord de la piscina más profunda del mundo hasta la fecha.</p>

                <p>Blue Abyss, encarada al entrenamiento de astronautas y proyectos espaciales, llegaba con sus 50 metros de profundidad sacando pecho ante Deepspot, la piscina polaca con hasta 45 metros de profundidad. Pero incluso antes de que Blue Abbys se haya abierto, Dubai ya ha inaugurado esta tremenda piscina que complementa de manera inversa su récord tocando los cielos, gracias al Burj Khalifa, el rascacielos más alto del mundo (al menos hasta que esa locura llamada Dubai Creek Tower esté construida).</p>


                    <p>Así como Blue Abbys superaba en 5 metros la profundidad de Deepspot (que hablando de sumergirse no es poco, ni mucho menos), la piscina Deep Dive Dubai llega a los 60 metros de profundidad. Un monstruo de 14 millones de litros de agua que el 27 de junio se llevaba su récord Guinness por se la piscina más profunda del mundo.</p>

                    <p>
                        Esta cantidad de agua rellena los 1.500 metros cuadrados de la estructura, siendo una piscina cubierta como las que hemos mencionado. Es más o menos el volumen de seis piscinas olímpicas, si bien la de la NASA aloja algo más de 22,7 millones de litros de agua (sin ser tan profunda).


                    </p>

                "
            )
        );

        return $posts;
    }


}