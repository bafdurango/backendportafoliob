<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<li>
	<ul><a href="{{ route('index') }}">Inicio</a></ul>
	<ul><a href="{{ route('image.delete-all') }}">Limpiar</a></ul>
	<ul><a href="{{ route('api.posts') }}">Api rest</a></ul>
	<ul>
		<img src="{{ asset('logo.png') }}">
	</ul>
</li>

<br>

<div class="container">
    @yield('content')
</div>



<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

</body>
</html>