@extends('layouts.main')


@section('content')

<form action="{{ route('image.watermark') }}" method="post" >

	<table>
		<tr>
			<td>
				x: <input type="text" name="x_porcentaje">
			</td>
			<td>
				y: <input type="text" name="y_porcentaje">
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit"value="Marca de agua">
				{{ csrf_field() }}
			</td>
		</tr>
	</table>

</form>

<br>
<br>

<form action="{{ route('image.cut') }}" method="post" >

	<table>
		<tr>
			<td>
				x: <input type="text" name="x_pieces">
			</td>
			<td>
				y: <input type="text" name="y_pieces">
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Cortar imagen">
				{{ csrf_field() }}
			</td>
		</tr>
	</table>

</form>

<br>
<br>

<form action="{{ route('index') }}" method="get" >

	<table>
		<tr>
			<td>
				Url: <input type="text" name="post_url">
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Copiar post">
				{{ csrf_field() }}
			</td>
		</tr>
	</table>

</form>

<br>
<br>

<table >
		
		@foreach($data['table'] as $tr)
		<tr>
			@foreach($tr as $td)
			<td>
				<img src="{{ $td }}" style="max-width: 100%;" >
			</td>
			@endforeach()
		</tr>
		@endforeach()
		
	
</table>

<br>
<br>
<br>

<form action="{{ route('image.upload') }}" method="post" enctype="multipart/form-data" >
	
	<input type="file" name="image">

	<br>
	<br>

	{{ csrf_field() }}

	<input type="submit" value="Guardar" name="">

</form>

<br>
<br>

{!! $data['html'] !!}



@endsection('content')