<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome');
//});

Route::get('/', ["as" => "index", "uses" => "ImageController@index"]);

Route::get('/image/delete-all', ["as" => "image.delete-all", "uses" => "ImageController@deleteAll"]);


Route::post('image/upload', ["as" => "image.upload", "uses" => "ImageController@upload"]);

Route::post('image/watermark', ["as" => "image.watermark", "uses" => "ImageController@watermark"]);

Route::post('image/cut', ["as" => "image.cut", "uses" => "ImageController@cut"]);

Route::get('api/posts', ["as" => "api.posts", "uses" => "ApiController@index"])->middleware(['cors']);

Route::get('api/post/{id}', ["as" => "api.post", "uses" => "ApiController@getPost"])->middleware(['cors']);


//Route::get('/hola', function () {
    ////echo "hola";
    //echo env('APP_KEY');
//});

//Route::get('/cache/clear', function () {
    //$exitCode = Artisan::call('cache:clear');
    //echo "hecho";
//});